uses Gtk
uses Gee

directorio_tts:string
directorio_trabajo:string
directorio_datos:string
lengua:string
ventana_principal:Ventana
texto_anterior:string
portapapeles:Gtk.Clipboard
lectura_portapapeles:bool=false
maxlen:int
reloj:uint
init
	Gtk.init(ref args)
	ventana_principal= new Ventana()
	ventana_principal.show_all()
	portapapeles = Gtk.Clipboard.get_for_display (ventana_principal.get_display (), Gdk.SELECTION_CLIPBOARD);
	texto_anterior = portapapeles.wait_for_text ();
	maxlen=1000
	Gtk.main()
	
class Ventana:Gtk.Window
	editor:TextView
	editorscroll:Gtk.ScrolledWindow 
	box: Box
	botones: Box
	botones2: Box
	boton_play:Button
	boton_idioma:Button
	boton_finaliza:Button
	boton_portapapel:Button
	t:Traduccion
	
	init
		
		texto_anterior=""
		lengua="eu"
		//directorio_tts=Shell.quote("/home/gontzal/Mahaigaina/programazioa/katahots/katahots-1.0/tts/")
		directorio_tts="/usr/share/katahots/tts/"
		directorio_trabajo=GLib.Environment.get_home_dir()+"/"
		directorio_datos="/usr/share/katahots/"
		t= new Traduccion(directorio_datos,"es")
		t.traduce()
		title="Katahots"
		editorscroll = new Gtk.ScrolledWindow (null, null);
		this.set_size_request(300,200)
		box= new Box(Gtk.Orientation.VERTICAL, 5)
		
		botones= new Box(Gtk.Orientation.HORIZONTAL, 5)
		botones2= new Box(Gtk.Orientation.HORIZONTAL, 5)
		
		editor= new Gtk.TextView ();
		editor.set_wrap_mode (Gtk.WrapMode.WORD);
		editor.buffer.text = "Katahotsekin irakurtzea gauza erreza da.";
		editorscroll.add(editor)
		
		boton_play= new Button()
		boton_play.set_image(new Image.from_file (directorio_datos+"player-play50.png"))
		boton_play.clicked.connect(on_play)
		botones.add(boton_play)
		
		
		boton_finaliza= new Button()
		boton_finaliza.set_image(new Image.from_file (directorio_datos+"player-stop50.png"))
		boton_finaliza.clicked.connect(on_finaliza)
		botones.add(boton_finaliza)
		
		boton_idioma= new Button.with_label(t.t("Euskera"))
		boton_idioma.clicked.connect(on_idioma)
		botones2.add(boton_idioma)
		
		boton_portapapel= new Button.with_label(t.t("Lector de portapapeles desactivado"))
		boton_portapapel.clicked.connect(on_portapapel)
		botones2.add(boton_portapapel)
		
		box.pack_start(editorscroll,true,true,5)
		box.pack_start(botones,true,true,1)
		box.pack_start(botones2,true,true,1)
		botones.homogeneous = true
		botones2.homogeneous = true
		box.homogeneous=false
		this.add(box)
		reloj=Timeout.add(1000, on_verifica)
		this.destroy.connect(on_destroy)
		
	def on_destroy()
		Source.remove (reloj)
		this.destroy()
		
	def on_verifica():bool
		if lectura_portapapeles
			portapapeles:Gtk.Clipboard = Gtk.Clipboard.get_for_display (ventana_principal.get_display (), Gdk.SELECTION_CLIPBOARD);
			texto_portapapeles:string = portapapeles.wait_for_text ();
			if texto_anterior!=texto_portapapeles
				
				if texto_portapapeles.length<maxlen
					this.escucha (texto_portapapeles)
					texto_anterior=texto_portapapeles
		return true
		
	def on_finaliza()
		try
			Process.spawn_command_line_sync ("killall aplay")
		except
			pass
	def on_portapapel(btn:Button)
		if lectura_portapapeles==false
			btn.set_label(t.t("Lector de portapapeles activado"))
			lectura_portapapeles=true
			texto_anterior = portapapeles.wait_for_text ();
		else
			btn.set_label(t.t("Lector de portapapeles desactivado"))
			lectura_portapapeles=false
			texto_anterior = portapapeles.wait_for_text ();
		
	def on_play()
		if editor.buffer.text.replace(" ","").replace("\n","").replace("\t","")!="" 
			var texto=editor.buffer.text
			escucha (texto)
	def on_idioma(btn:Button)
		if btn.get_label()=="Euskera"
			btn.set_label("Español")
			lengua="es"
		else
			btn.set_label("Euskera")
			lengua="eu"
	def escucha(texto:string)
		//print texto.length.to_string()
		print texto
		crear_archivo_input(texto)
		try
			
			Process.spawn_command_line_sync (
			directorio_tts+"tts"+
			" -InputFile="+directorio_trabajo+"input-katahots.txt"+
			" -Lang="+lengua+
			" -DataPath="+directorio_tts+"data_tts"+
			" -OutputFile="+directorio_trabajo+"Output-katahots.wav")
			
			Process.spawn_command_line_async ( "aplay "+directorio_trabajo+"Output-katahots.wav" )
			
		except
			print "error en la reproducción"
		
	def crear_archivo_input(texto:string)
		
		var f1 = FileStream.open (directorio_trabajo+"input-katahots.txt", "w")
		var texto1=""
		try
			texto1=convert (texto, texto.length, "ISO-8859-1", "utf-8")
		except
			pass
		f1.puts(texto1)
		
	
